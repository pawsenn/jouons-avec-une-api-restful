import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Character from '../views/Character.vue'
import Location from "@/views/Location.vue";
import Episode from "@/views/Episode.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/character/:id',
      name: 'character',
      component: Character,
      props: true
    },
    {
      path: '/location/:id',
      name: 'location',
      component: Location,
      props: true
    },
    {
      path: '/episode/:id',
      name: 'episode',
      component: Episode,
      props: true
    },
  ]
})

export default router
