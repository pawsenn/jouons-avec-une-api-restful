import {defineStore} from 'pinia'

export const useEpisodeStore = defineStore({
  id: 'episode',
  state: () => {
    return {
      episodes: [],
      episode:{},
      loading: false,
      error: null
    }
  },

  actions: {
    async fetchEpisodes() {
      this.episodes = []
      this.loading = true
      try {
        const json = await fetch('https://rickandmortyapi.com/api/episode')
            .then((response) => response.json());
        this.episodes = json.results
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.episodes
    },

    async getAndSetEpisodeById(id: number) {
      let episode = {}
      this.loading = true
      try {
        episode = await fetch(`https://rickandmortyapi.com/api/episode/${id}`)
            .then((response) => response.json())
        this.episode = episode
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.episode
    },

    async getEpisodeById(id: number) {
      let episode = {}
      this.loading = true
      try {
        episode = await fetch(`https://rickandmortyapi.com/api/episode/${id}`)
            .then((response) => response.json())
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return episode
    },
  }
})
