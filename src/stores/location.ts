import {defineStore} from 'pinia'

export const useLocationStore = defineStore({
  id: 'location',
  state: () => {
    return {
      locations: [],
      location:{},
      loading: false,
      error: null
    }
  },

  actions: {
    async fetchLocations() {
      this.locations = []
      this.loading = true
      try {
        const json = await fetch('https://rickandmortyapi.com/api/location')
            .then((response) => response.json());
        this.locations = json.results
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.locations
    },

    async getAndSetLocationById(id: number) {
      let location = {}
      this.loading = true
      try {
        location = await fetch(`https://rickandmortyapi.com/api/location/${id}`)
            .then((response) => response.json())
        this.location = location
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.location
    },

    async getLocationById(id: number) {
      let location = {}
      this.loading = true
      try {
        location = await fetch(`https://rickandmortyapi.com/api/location/${id}`)
            .then((response) => response.json())
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return location
    },
  }
})
