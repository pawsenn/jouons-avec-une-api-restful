import {defineStore} from 'pinia'

export const useCharacterStore = defineStore({
  id: 'character',
  state: () => {
    return {
      characters: [],
      next: null,
      prev: null,
      character: {},
      loading: false,
      error: null
    }
  },

  actions: {
    async fetchCharacters(url: string = "https://rickandmortyapi.com/api/character") {
      this.characters = []
      this.loading = true
      try {
        const json = await fetch(url)
            .then((response) => response.json());
        this.characters = json.results
        this.next = json.info.next;
        this.prev = json.info.prev;
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.characters
    },

    async getAndSetCharacterById(id: number) {
      let character = {}
      this.loading = true
      try {
        character = await fetch(`https://rickandmortyapi.com/api/character/${id}`)
            .then((response) => response.json())
        this.character = character
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return this.character
    },

    async getCharacterById(id: number) {
      let character = {}
      this.loading = true
      try {
        character = await fetch(`https://rickandmortyapi.com/api/character/${id}`)
            .then((response) => response.json())
      } catch (error: any) {
        this.error = error
      } finally {
        this.loading = false
      }
      return character
    },
  }
})
