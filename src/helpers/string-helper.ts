export const getIdFromUrl = (url: string) => {
    return url.split("/").pop();
}