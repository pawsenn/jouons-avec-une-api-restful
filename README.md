# rickmortydb

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
## Consignes
Implémenter une interface graphique consommant une API donnée.

## Exercice API Rick & Morty
Une API existe contenant des données sur les personnages de la série connue **Rick and Morty**.
 - [https://rickandmortyapi.com/api](https://rickandmortyapi.com/api)
 - [Documentation de l'api](https://rickandmortyapi.com/documentation/#rest)
 
Consommez cette API avec la technologie de votre choix.
Au sein de l'application ainsi implémentée votre API devra : 
	- Lister l'ensemble des personnages
	- Avoir une fonctionnalité de pagination pour voir les différents personnages
	- Voir la fiche d'un personnage
